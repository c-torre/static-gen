Static Site Generator
=====================

Simple POSIX-compliant shell script to make a static site.
Fork from [kisslinux/website](https://github.com/kisslinux/website).

Usage
-----

1. Write a `header.html` with e.g. HTML headers and CSS.
1. Write a `footer.html` with e.g. license information.
1. Add markdown files under `src/` with [`smu`](https://github.com/karlb/smu) syntax e.g. `index.md`.
1. Run `$ sh make`. This step concatenates header + parsed markdown + footer.
1. Output files (HTML and txt) located at `site/`.

The simple script design prevents useless features.

License
-------

GPLv3
