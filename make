#!/bin/sh -e
#
# Simple static site builder


wiki_nav() {
    # Generate the navigation bar and edit information for each Wiki page.

    # Split the path on '/'.
    # shellcheck disable=2086
    {
        set -f; IFS=/
        set +f ${page##./}
        unset IFS
    }

    # '$nar' contains the generated nav without HTML additions for use in
    # length calculations below.
    nav="<a href=\"/$1\">$1</a>" nar=$1

    [ "$2" ] &&             nav="$nav / <a href='/$1/$2'>$2</a>" nar="$nar / $2"
    [ "${3#index.md}" ] && nav="$nav / ${3%%.md}" nar="$nar / ${3%%.md}"

    # Calculate the amount of padding to add. This will right align the
    # edit page link. Wiki page length is always 80 columns.
    nav="$nav$(printf '%*s' "$((80 - ${#nar} - 14))" "")"
    # nav="$nav<a href='$wiki_url/edit/master/${page##*wiki/}'>Edit this page</a>"

    printf '%s\n\n%s\n\n\n' "$nav" \
        "$(git submodule foreach --quiet git log -1 \
        --format="Edited (<a href='$wiki_url/commit/%H'>%h</a>) at %as by %an" \
        "${page##*wiki/}")"
}

page() {
    pp=${page%/*} title=${page##*/} title=${title%%.md}

    mkdir -p "site/$pp"

    # If the title is index.txt, set it to the parent directory name.
    # Example: /wiki/index.txt (index) -> (wiki).
    case $title in index) title=${pp##*/} ;; esac
    case $title in .)     title=home ;; esac

    # GENERATION STEP.
    case $page in
        # Generate HTML from Wiki pages
        */wiki/index.md)
            smu < "src/$page" | cat header.html - footer.html > "site/${page%%.md}.html"
        ;;

        */wiki/*.md)
            wiki_nav | cat - "src/$page" | smu | cat header.html - footer.html > "site/${page%%.md}.html"
        ;;

        # Generate HTML from markdown files
        *.md)
            smu < "src/$page" | cat header.html - footer.html > "site/${page%%.md}.html"
        ;;

        # Copy over any non-txt files.
        *)
            cp -f "src/$page" "site/$page"
        ;;
    esac

    # POST-GENERATION STEP
    case $page in
        # Hardlink all .txt files to the site/ directory.
        *.md) ln -f "src/$page" "site/${page%%.md}.txt" ;;
    esac
}

main() {
    wiki_url=https://blog.ctorre.me/
    repo_url=https://gitlab.com/c-torre

    rm -rf site
    mkdir -p site

    (cd src && find . -type f) | while read -r page; do
        printf '%s\n' "CC $page"
        page "$page"
    done
}

main "$@"
